package org.green.ftp.camel.route;

import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.green.ftp.camel.processor.FtpFileConsumerProcessor;
import org.green.ftp.camel.processor.filter.LastModifiedDatePredicate;
import org.springframework.stereotype.Component;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;


@Component
public class FtpRouteToBean extends RouteBuilder {
    @Override
    public void configure() throws Exception {

        String username = "root";
        String password = "spanner";
        String ipAddress = "192.168.40.102";

        // https://stackoverflow.com/questions/45759853/changing-directory-from-home-to-root-in-camel-ftp
        // This sftp only takes relative path. To get around that we can use ../ to change directory and navigate to the directory we wanted. Without the ../ the directory below would be like: /root//mnt/data/volumes/ipfbackend/proximityanalysis/Input
        from("sftp://" + username + "@" + ipAddress + ":22/../mnt/data/volumes/ipfbackend/proximityanalysis/Output?password=" + password +
             "&move=.done&useUserKnownHostsFile=false&include=.*\\.json$")
                .streamCaching()
                .filter(new LastModifiedDatePredicate(LocalDateTime.of(2121, 03, 14, 00, 00, 00)))
                .process(new FtpFileConsumerProcessor("D:\\opt\\ipf\\arcgis\\ProximityAnalysis\\Output"));
    }
}
