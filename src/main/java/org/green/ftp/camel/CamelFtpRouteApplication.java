package org.green.ftp.camel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelFtpRouteApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelFtpRouteApplication.class, args);
	}

}
