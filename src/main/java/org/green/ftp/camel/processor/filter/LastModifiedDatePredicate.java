package org.green.ftp.camel.processor.filter;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class LastModifiedDatePredicate implements Predicate {
    private static final Logger logger = LoggerFactory.getLogger(LastModifiedDatePredicate.class);

    private LocalDateTime oldestAllowedDate;

    public LastModifiedDatePredicate(LocalDateTime oldestAllowedDate) {
        this.oldestAllowedDate = oldestAllowedDate;
    }

    @Override
    public boolean matches(Exchange exchange) {
        Long epoch = (Long) exchange.getIn().getHeader("CamelFileLastModified");
        LocalDateTime date = Instant.ofEpochMilli(epoch).atZone(ZoneId.systemDefault()).toLocalDateTime();
        logger.info("file date: {}, Limit: {}", date, oldestAllowedDate);
        return date.isAfter(oldestAllowedDate);
    }
}
