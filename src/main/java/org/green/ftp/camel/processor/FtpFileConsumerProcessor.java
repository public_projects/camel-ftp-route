package org.green.ftp.camel.processor;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.tomcat.util.http.fileupload.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;


@Slf4j
public class FtpFileConsumerProcessor implements Processor {

    private final File outputDirectoryFile;

    public FtpFileConsumerProcessor(String outputDirectory) throws Exception {
        outputDirectoryFile = new File(outputDirectory);

        if (!outputDirectoryFile.isDirectory()) {
            throw new Exception("Given directory not valid");
        }
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        String fileName = (String) exchange.getIn().getHeader("CamelFileName");
        log.info("File name: {}", fileName);

        InputStream inputStream = exchange.getIn().getBody(InputStream.class);

        File targetFile = new File(outputDirectoryFile.getAbsolutePath() + File.separator + fileName);
        log.info("New File: {}", targetFile.getAbsolutePath());
        try (FileOutputStream outputStream = new FileOutputStream(targetFile)) {
            int read;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        }
    }
}
